# GitNote - Meine erste Notiz

Dies ist ein Notizbuch, das unter der Kontrolle von [GitNote](https://bitbucket.org/juengling/gitnote) steht. Wenn alles korrekt installiert wurde, brauchst du nur noch mit einem Markdown-Editor deine Notizen zu bearbeiten.

Zwischen dem Betriebssystem und GitNote besteht dabei folgende Beziehung:

* Datei = GitNote "Notiz"
* Verzeichnis = GitNote "Notizbuch"

---

# Automatischer Ablauf

GitNote erkennt automatisch, wenn eine Datei

* hinzugefügt
* geändert
* gelöscht

wurde. Ungefähr im Minutenrhythmus wird dies überprüft und in Git *committet*. Dadurch hast du nicht nur die Datei gesichert, sondern auch automatisch eine Änderungshistorie. Schlimmstenfalls kann so ein alter Stand einer Datei wiederhergestellt werden.
