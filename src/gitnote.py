'''
Gitnote

Created on 07.09.2017

@author: chris
'''
from argparse import ArgumentParser
from pprint import pprint
import logging
import os
import socket
import sys
import time

from git import Repo


__APP_NAME__ = 'gitnote'
__APP_VERSION__ = '0.1.0'

__RETURN_OK__ = 0


def main(argv):
    '''
    Gitnote intercepts given folder hierarchy for added and
    removed files in an endless loop.
    So don't expect this program to end by itself :-)
    '''

    args = parse_command_line(argv)

    if args.verbose >= 1:
        print('Folder: {folder}'.format(folder=args.folder))

    logger = logging.getLogger(__APP_NAME__)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    fh = logging.FileHandler(args.logfile)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    logger.debug('Starting {}'.format(__APP_NAME__))

    logger.debug('Retrieve host name ...')
    computername = socket.gethostname()
    logger.debug('Hostname = %s', computername)

    #git_update(args.folder, computername)
    # Get access to the repository
    repo = Repo(args.folder)

    try:
        logger.debug('Get access to remote "origin"')
        origin = repo.remote('origin')
    except ValueError as ex:
        logger.debug('Remote "origin" doesn\'t exist')
        origin = None

    index = repo.index
    message = 'Auto commit on {}'.format(computername)

    while True:
        logger.debug('ping')

        # TODO: Save previous loop's file list

        # Add all currently existing files to a dict
        files = {}
        for file in getfiles(args.folder):
            files.update(file)

        docommit = False

        # TODO: Check if any of these file is new or newer than in previous loop
        # index.add(file)
        # docommit=True

        # TODO: Check if any of the previous loop's file has been deleted
        # index.add(file)
        # docommit=True

        # Commit with automatic message, avoiding empty commits
        if docommit:
            index.commit(message)

        # If possible, push to the "origin" remote
        try:
            if origin:
                origin.push()
        except Exception as ex:
            logger.error('Cannot push.')
            logger.exception(ex)

        time.sleep(args.interval * 60)


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments without program name
    :return: Arguments object
    '''

    parser = ArgumentParser(prog=__APP_NAME__ + ' v' + __APP_VERSION__,
                            description=main.__doc__)

    parser.add_argument('folder',
                        help='Specify folder to intercept')

    parser.add_argument('--logfile', default='gitnote.log',
                        help='Specify log file (default = gitnote.log)')

    parser.add_argument('--interval', type=int, default=1, metavar='MINUTES',
                        help='Interval between two folder checks in minutes, default = 1')

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Increase verbosity level')

    args = parser.parse_args(arguments)
    return args


def getfiles(folder):
    '''
    Generator, returning only files and there modification time in folder hierarchy

    :param folder: Start folder to intercept
    '''

    for root, subFolders, files in os.walk(folder):
        if '.git' in root:
            continue

        for file in files:
            fullpath = os.path.join(root, file)
            if os.path.isfile(fullpath):
                modtime = os.path.getmtime(fullpath)
                yield {fullpath: modtime}


def intercept(folder):
    path_to_watch = folder
    before = dict([(f, None) for f in os.listdir(path_to_watch)])
    while True:
        after = dict([(f, None) for f in os.listdir(path_to_watch)])
        added = [f for f in after if not f in before]
        removed = [f for f in before if not f in after]
        changed = []

        if added:
            print("Added: ", ", ".join(added))
        if removed:
            print("Removed: ", ", ".join(removed))
        if changed:
            print("Removed: ", ", ".join(changed))

        before = after


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
