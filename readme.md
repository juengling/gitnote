# Gitnote

Gitnote is an experimental project to create a possibility to edit notes on one of my computers, and then distribute it to any other of my computers. The concept is well-known by [Evernote](https://evernote.com) or Microsoft's [OneNote](https://www.onenote.com/). Any part of this project shall consist of open-source applications and code.

The basic idea is to

- create notes with a [Markdown](https://de.wikipedia.org/wiki/Markdown) editor, e.g. the really remarkable [Remarkable](https://remarkableapp.github.io) 
- store each note in one file (`*.md`)
- organize notes in notebooks, represented by a directory/folder hierarchy (you may consider a folder as a *notebook*, and a file as a *note*, but there is no compulsion to do so)
- automatically save and share notes with [Git](https://git-scm.com/)
- store the central repository in any self-hosted Git server, e.g. [Gitlab](https://about.gitlab.com/)

Ideally the user only needs to learn Markdown, but even this is not really necessary, as any other file format will perhaps do the job. You should desperately refrain from using licenced applications, as there file format may change. 

There is no need to learn Git and its concepts, because anything regarding Git shall be done in the background. But anyway this is not the worst thing you may want to learn :-)

And last but not least there is no need to think about sharing and updating, as this shall be done by Gitnote itself in the background. You just will have to save your notes regularly, unless you find an editor which can do that for you, too.